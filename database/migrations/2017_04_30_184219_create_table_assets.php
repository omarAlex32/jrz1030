<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('variation_of_id')->nullable();
            $table->integer('acount_id');
            $table->integer('user_id');
            $table->string('title');
            $table->string('slug');
            $table->string('description')->nullable();
            $table->string('type', 50);
            $table->string('mime_type', 50)->nullable();
            $table->string('path')->nullable();
            $table->string('hash')->nullable();
            $table->double('size', 15, 4)->nullable();
            $table->boolean('is_link');
            $table->boolean('is_public');
            $table->integer('session_type_enum')->nullable();
            $table->text('metadata')->nullable();
            $table->string('source');
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
