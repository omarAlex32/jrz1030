<?php

use Illuminate\Database\Seeder;

class AssetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('assets')->insert([
            
            'id'=>'1',
            'variation_of_id'=>null,
            'acount_id'=> '1',
            'user_id'=>'1',
            'title'=>'imagen.jpg',
            'slug'=>'123',
            'description'=>'una imagen',
            'type'=>'image',
            'mime_type'=>'img',
            'path'=>'root/imagenes/',
            'hash'=>null,
            'size'=>'5000',
            'is_link'=>false,
            'is_public'=>true,
            'session_type_enum'=>'1',
            'metadata'=>null,
            'source'=>'uploaded',
            'created_at'=>date('Y-m-d H:i:s')
 		]);

         DB::table('assets')->insert([
            
            'id'=>'2',
            'variation_of_id'=>null,
            'acount_id'=> '1',
            'user_id'=>'1',
            'title'=>'imagen2.jpg',
            'slug'=>'123',
            'description'=>'una imagen',
            'type'=>'image',
            'mime_type'=>'img',
            'path'=>'root/imagenes/',
            'hash'=>null,
            'size'=>'5000',
            'is_link'=>false,
            'is_public'=>true,
            'session_type_enum'=>'1',
            'metadata'=>null,
            'source'=>'uploaded',
            'created_at'=>date('Y-m-d H:i:s')
 		]);

         DB::table('assets')->insert([
            
            'id'=>'3',
            'variation_of_id'=>null,
            'acount_id'=> '1',
            'user_id'=>'1',
            'title'=>'video_from_youtube',
            'slug'=>'123',
            'description'=>'un video',
            'type'=>'video',
            'mime_type'=>'img',
            'path'=>'root/videos/',
            'hash'=>null,
            'size'=>'5000',
            'is_link'=>false,
            'is_public'=>true,
            'session_type_enum'=>'1',
            'metadata'=>null,
            'source'=>'uploaded',
            'created_at'=>date('Y-m-d H:i:s')
 		]);
    }
}
