$( document ).ready(function() {

  jQuery.noConflict();
  
  loadPrimaryTree();

})

// load primary tree
  function loadPrimaryTree(){
    jQuery("#tree_result").html('');
    jQuery.ajax({
      type:'GET',
      url:'/loadtree',
      data:'_token = <?php echo csrf_token() ?>',
      success:function(result){
        for (var i = 0; i < result.length; i++) {
          jQuery("#tree_result").append('<details><summary class="summ-'+result[i].id+'" onclick="getNodesNFiles('+result[i].id+');">'+result[i].title+'</summary><div state="closed" class="node-'+result[i].id+'" style="margin-left:15px;"></div></details>');
        }
      },
      error: function(e){
        alert('some error has ocurred');
        console.log('error');
      }
    });
  }
  // end of load primary tree

// show / hide tree nodes and folders content
function getNodesNFiles(id){
  getnodes(id);
  getAssets(id);
  getFolders(id);
}

// load tree nodes
function getnodes(id){
  var state = jQuery('.node-'+id).attr('state');
  jQuery('.activeNode').removeClass('activeNode');
  jQuery('.summ-'+id).addClass('activeNode');
  app.selection=[];
  if(state == 'closed'){
    jQuery('.node-'+id).attr('state', 'open');  
    jQuery.ajax({
      type:'GET',
      url:'/loadtree',
      data:'_token = <?php echo csrf_token() ?>' + '&id='+ id,
      success:function(result){
        if(result.length==0){
          // jQuery('.node-'+id).html('...');
        } 
        for (var i = 0; i < result.length; i++) {
          jQuery('.node-'+id).append('<details><summary class="summ-'+result[i].id+'"  onclick="getNodesNFiles('+result[i].id+');">'+result[i].title+'</summary><div state="closed" class="node-'+result[i].id+'" style="margin-left:15px;"></div></details>');
        }
      },
      error: function(e){
        console.log('error');
        alert('an error has ocurred');
      }
    });
  }else{
    jQuery('.node-'+id).attr('state','closed'); 
    jQuery('.node-'+id).html(''); 
  }
}

// load files
function getAssets(id){
  jQuery.ajax({
    type:'GET',
    url:'/assets/get',
    data:'_token = <?php echo csrf_token() ?>' + '&id='+ id,
    success:function(result){
      app.folder.id = id;
      app.folder.title = result.folder.title;
      app.folder.type = result.folder.folder_type_enum;    
      result.asset.forEach(function(item, index, arr){
        arr[index].isSelected=false;
        arr[index].father=[{'title':''}]
      })
      app.files = result.asset;
      app.selection = [];
      // app.selectionSize = 0;
    },
    error: function(e){
      console.log('error');
    }
  });
}

function getFolders(id){
  jQuery.ajax({
    type:'GET',
    url:'/folders/get',
    data:'_token = <?php echo csrf_token() ?>' + '&id='+ id,
    success:function(result){   
      result.folder.forEach(function(item, index, arr){
        arr[index].isSelected=false
      })
      app.folders = result.folder;
      app.selection = [];
      // app.selectionSize = 0;
    },
    error: function(e){
      console.log('error');
    }
  });
}










