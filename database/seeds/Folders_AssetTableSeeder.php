<?php

use Illuminate\Database\Seeder;

class Folders_AssetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('asset_folder')->insert([
            
            'asset_id'=>'1',
            'folder_id'=>'1',
            'created_at'=> date('Y-m-d H:i:s')
 		]);

         DB::table('asset_folder')->insert([
            
            'asset_id'=>'2',
            'folder_id'=>'1',
            'created_at'=> date('Y-m-d H:i:s')
        ]);

         DB::table('asset_folder')->insert([
            
            'asset_id'=>'3',
            'folder_id'=>'2',
            'created_at'=> date('Y-m-d H:i:s')
        ]);


    
    }
}
