<?php

use Illuminate\Database\Seeder;

class FoldersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('folders')->insert([
            
            'account_id' => '001',
            'user_id'=>'001',
            'parent_id'=>null,
            'title'=>'Public',
            'slug'=>'trtr',
            'description'=>'public folder',
            'size'=>'10.5',
            'item_count'=>'2',
            'folder_type_enum'=>'1',
            'created_at'=> date('Y-m-d H:i:s')
 		]);

         DB::table('folders')->insert([
            
            'account_id' => '001',
            'user_id'=>'001',
            'parent_id'=>null,
            'title'=>'Shared',
            'slug'=>'trtr2',
            'description'=>'shared folder',
            'size'=>'5.5',
            'item_count'=>'1',
            'folder_type_enum'=>'3',
            'created_at'=> date('Y-m-d H:i:s')
 		]);

         DB::table('folders')->insert([
            
            'account_id' => '001',
            'user_id'=>'001',
            'parent_id'=>null,
            'title'=>'Team shared',
            'slug'=>'trtr2',
            'description'=>'team shared folder',
            'size'=>'5.5',
            'item_count'=>'1',
            'folder_type_enum'=>'5',
            'created_at'=> date('Y-m-d H:i:s')
 		]);

         DB::table('folders')->insert([
            
            'account_id' => '001',
            'user_id'=>'001',
            'parent_id'=>null,
            'title'=>'User',
            'slug'=>'trtr2',
            'description'=>'team shared folder',
            'size'=>'15',
            'item_count'=>'4',
            'folder_type_enum'=>'4',
            'created_at'=> date('Y-m-d H:i:s')
 		]);

         DB::table('folders')->insert([
            
            'account_id' => '001',
            'user_id'=>'001',
            'parent_id'=>null,
            'title'=>'Trash',
            'slug'=>'trtr2',
            'description'=>'team shared folder',
            'size'=>'0',
            'item_count'=>'0',
            'folder_type_enum'=>'2',
            'created_at'=> date('Y-m-d H:i:s')
 		]);



         // on public folder
         DB::table('folders')->insert([
            'parent_id' => '1',
            'account_id' => '001',
            'user_id'=>'001',
            'title'=>'Images',
            'slug'=>'img',
            'description'=>'images folder',
            'size'=>'0',
            'item_count'=>'0',
            'folder_type_enum'=>'0',
            'created_at'=> date('Y-m-d H:i:s')
        ]);
         // on public folder
         DB::table('folders')->insert([
            'parent_id' => '1',
            'account_id' => '001',
            'user_id'=>'001',
            'title'=>'Videos',
            'slug'=>'videos',
            'description'=>'videos folder',
            'size'=>'0',
            'item_count'=>'0',
            'folder_type_enum'=>'0',
            'created_at'=> date('Y-m-d H:i:s')
        ]);
         // on imagenes folder
         DB::table('folders')->insert([
            'parent_id' => '6',
            'account_id' => '001',
            'user_id'=>'001',
            'title'=>'Gif',
            'slug'=>'gif',
            'description'=>'gif folder',
            'size'=>'0',
            'item_count'=>'0',
            'folder_type_enum'=>'0',
            'created_at'=> date('Y-m-d H:i:s')
        ]);
         // on imagenes folder
         DB::table('folders')->insert([
            'parent_id' => '6',
            'account_id' => '001',
            'user_id'=>'001',
            'title'=>'Png',
            'slug'=>'png',
            'description'=>'png folder',
            'size'=>'0',
            'item_count'=>'0',
            'folder_type_enum'=>'0',
            'created_at'=> date('Y-m-d H:i:s')
        ]);

         // on gif folder
         DB::table('folders')->insert([
            'parent_id' => '8',
            'account_id' => '001',
            'user_id'=>'001',
            'title'=>'Animated',
            'slug'=>'animated',
            'description'=>'gif animados folder',
            'size'=>'0',
            'item_count'=>'0',
            'folder_type_enum'=>'0',
            'created_at'=> date('Y-m-d H:i:s')
        ]);
         // on shared folder
         DB::table('folders')->insert([
            'parent_id' => '2',
            'account_id' => '001',
            'user_id'=>'001',
            'title'=>'Documents',
            'slug'=>'compartido',
            'description'=>'folder compartido',
            'size'=>'0',
            'item_count'=>'0',
            'folder_type_enum'=>'0',
            'created_at'=> date('Y-m-d H:i:s')
        ]);
    }
}
