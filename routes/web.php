<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/loadtree','FoldersController@loadTree');



Route::get('/folders/get','FoldersController@getFolders');

Route::post('/folders/create', 'FoldersController@store');

Route::post('/folders/rename', 'FoldersController@edit');

Route::get('/folders/cut', 'FoldersController@cut');




Route::get('/assets/download/{id}', 'AssetsController@download');

Route::get('/assets/get','AssetsController@getAssets');

Route::post('/assets/upload', 'AssetsController@store');

Route::post('assets/search', 'AssetsController@searchAssets');

Route::post('/assets/rename', 'AssetsController@edit');

Route::get('/assets/copy', 'AssetsController@copy');

Route::get('/assets/cut', 'AssetsController@cut');

Route::get('/assets/delete', 'AssetsController@delete');


