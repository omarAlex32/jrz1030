<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('css/styles.css') }}" rel="stylesheet" type="text/css" />
	

	<script type="text/javascript" src="{{ asset('js/bootbox.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/bootstrap.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset('js/axios.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/vue.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/vueMain.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/vueComponents.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/jquery.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/ajaxFunctions.js')}}"></script>

	<meta name="_token" content="{!! csrf_token() !!}" />
</head>
<body>
	
		

</body>
</html>