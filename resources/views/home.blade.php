@extends('layout')
@section('title','Home')

<!-- ////////////////////////////////////////////////////////////////////////////////// -->
<!-- ///////// meta for ajax requests -->
<meta name="csrf-token" content="{{ csrf_token() }}" />

<!-- ////////////////////////////////////////////////////////////////////////////////// -->
<!-- ///////// start of principal container-->
<div class="container">
	<div class="row">
		<!-- /////////////////////////////////////////////////////////////////////////////////// -->
		<!-- //////// start of tree / search div -->
		<div class="nav_tree col">
		
			<ul class="nav nav-tabs">
  				<li role="presentation" :class="{'active' : !isSearching}"><a class="btn" @click="switchSearching(true)">Tree</a></li>
				<li role="presentation" :class="{'active' : isSearching}"><a class="btn" @click="switchSearching(false)">Search</a></li>
			</ul>	
			
			<br>
			
			<!-- ///////// tree -->
			<div id="tree_result" v-show="!isSearching">
			</div>
			
			<!-- ///////// search -->
			<div v-show="isSearching">
					<form @submit.prevent = "search()" id="search_form" action="">
				<div class="input-group">
						<input type="text" class="form-control" name="searchFor"  placeholder="Search for..." required>
						<span class="input-group-btn">
							<button type="submit" class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Go</button>
						</span>
				</div>
					</form>
			</div>

		</div>
		<!-- /////////////////////////////////////////////////////////////////////////////////// -->
		<!-- //////// end of tree / search div -->

		<!-- /////////////////////////////////////////////////////////////////////////////////// -->
		<!-- //////// start of content / search result div -->
		<div class="content col-8"  @click="clearSelection()">
			<div class="page-header">
				<h3 class=" display-4" >@{{folder.title}}</h3>
			</div>
			<div  id="content_result">


				<div class="assetsFilesContainer">
					<div v-for="folder in folders">	
						<folder :view="viewVar" :searching="isSearching" :folder="folder" :key="folder" v-on:dblclick.native="goToFolder(folder.id)"></folder>
					</div>
				</div>

				<!-- if showVar is true shows icons view, else show list view -->
				<div v-show="!viewVar||isSearching" class="text-left">
					<div :class="{'col-xs-5':!isSearching, 'col-xs-4':isSearching}"><strong>Name</strong></div>
					<div :class="{'col-xs-3':!isSearching, 'col-xs-2':isSearching}"><strong>Type</strong></div>
					<div :class="{'col-xs-4':!isSearching, 'col-xs-3':isSearching}"><strong>Size</strong></div>
					<div v-show="isSearching" :class="{'col-xs-4':!isSearching, 'col-xs-3':isSearching}"><strong>Folder</strong></div>
					<br>	
				</div>	


				<div :class="{'assetsFilesContainer': viewVar&&!isSearching}">
					<div v-for="file in files">	
						<asset :view="viewVar" :searching="isSearching" :asset="file" :key="file" v-on:dblclick.native="downloadAsset(file.id)"></asset>
					</div>
				</div>
					<!-- list view -->
					
				<div class="alert alert-info" v-show="showContenMessage">
 					<strong>No files</strong>
				</div>
			</div>
		</div>
		<!-- /////////////////////////////////////////////////////////////////////////////////// -->
		<!-- //////// end of content / search result div -->

		<!-- //////// upload modal div -->		
		<upload-modal :title="folder.title"  :father_id="folder.id" v-if="showUploadModalVar" @close="showUploadModalVar=false"></upload-modal>

		<!-- //////// create folder modal div -->		
		<create-folder-modal :title="folder.title"  :father_id="folder.id" v-if="showCreateFolderModalVar" @close="showCreateFolderModalVar=false"></create-folder-modal>
		
		<!--////////rename modal div -->
		<rename-modal :title="selection[0].title" :item_description="selection[0].description" :item_type="selection[0].itemType" :item_id="selection[0].id" v-if="showRenameModalVar" @close="showRenameModalVar=false"></rename-modal>
		
		<!-- /////////////////////////////////////////////////////////////////////////////////// -->
		<!-- //////// start of options and view menu -->
		<div class="options" >	
			<div class="btn-group">
				<div class="btn-group">
					<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
						<span class="glyphicon glyphicon-th"></span></button>
						<ul class="dropdown-menu dropdown-menu-right" role="menu">
							<li><a href="#" :disabled="isSearching" class="btn" @click="viewVar=true"><p class="text-left">Icons</p></a></li>
							<li><a href="#" class="btn" @click="viewVar=false"><p class="text-left">List</p></a></li>
						</ul>
				</div>
				<div class="btn-group">
					<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
						 <span class="glyphicon glyphicon-menu-hamburger"></span></button>
						<ul class="dropdown-menu dropdown-menu-right" role="menu">
							<li class="dropdown-header">Folder options</li>   
							<li><a class="btn" :disabled="!isInFolder" @click="showModal('upload')"><p class="text-left">Upload file</p></a></li>
							<li><a class="btn" :disabled="!isInFolder" @click="showModal('create_folder')"><p class="text-left">Create folder</p></a></li>
							<li><a class="btn" v-show="!isTrashFolder" :disabled="isTrashFolder"><p class="text-left">Empty trash</p></a></li>
							
							<li class="divider"></li>
							<li class="dropdown-header">Selection options</li> 
							<li><a class="btn" @click="clipboardActions('cut')" :disabled="isFileSelected"><p class="text-left">Cut</p></a></li>
							<li><a class="btn" @click="clipboardActions('copy')" :disabled="isFileSelected"><p class="text-left">Copy</p></a></li>
							<li><a class="btn" @click="clipboardActions('paste')" :disabled="isClipboardEmpty"><p class="text-left">Paste</p></a></li>
							<li><a class="btn" @click="deleteItem()" :disabled="isFileSelected"><p class="text-left">Delete</p></a></li>
							<li><a class="btn" @click="showModal('rename')" :disabled="!multipleFiles"><p class="text-left">Rename</p></a></li>
							
							<li class="divider"></li>
							<li><a class="btn" @click="selectAll()" :disabled="!areFiles"><p class="text-left">Select all</p></a></li> 
							<li><a class="btn" @click="clearSelection()" :disabled="isFileSelected"><p class="text-left">Clear selection</p></a></li> 
							 
						</ul>
				</div>
			</div>
		</div>	

		<!-- /////////////////////////////////////////////////////////////////////////////////// -->
		<!-- //////// end of options and view menu -->


		<!-- /////////////////////////////////////////////////////////////////////////////////// -->
		<!-- //////// selection info div -->
		<div class="selection-info">
			<p>@{{selection.length}} items selected</p>
			<p>Clipboard: @{{clipboard.items.length}}</p>
		</div>

	</div>
</div>	
<!-- ////////////////////////////////////////////////////////////////////////////////// -->
<!-- ///////// end of principal container-->





<!-- /////////////////////////////////////////////////////////////////////////////////// -->
<!-- //////// jquery-->
<script type="text/javascript" src="{{ asset('js/jquery.js')}}"></script>
