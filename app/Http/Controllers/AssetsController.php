<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Asset;
use App\Folder;

class AssetsController extends Controller
{

    public function store(Request $request)
    {

        //validate form data
        $this->validate(request(),[
            'file' => 'required',
            'file_name' => 'required',
            'file_description' => 'required',
            ]);

        //variables initialization
    	$file=$request->file('file');
        $father_id = $request->input('father_id');
        $description = $request->input('file_description');
        $ext =$file->guessClientExtension();
        // $title = $request->input('file_name').'.'.$ext;
        $title = $request->input('file_name');
        $type = "other";
        $acount_id = "1";
        $user_id = '1';
        $slug = "123";
        $mimetype = $file ->getMimeType();
        $size = $file->getClientSize();
        $is_link = false; 
        $is_public = false; 
        if($father_id == '1'){ $is_public = true;}
        $session_type_enum = '123';
        $metadata_json=null;
        $source= 'upload';
        $created_at = date('Y-m-d H:i:s');
       
        $img_array = array("jpg", "jpeg", "png", "gif", "png");
        $document_array = array("txt", "pdf", "doc", "xls", "ppt", "rtf", "css");
        if(in_array($ext, $img_array)){
            $type = "image";  
            list($width, $height) = getimagesize($file);
            $metadata =  array('width' => $width, 'height' =>$height);
            $metadata_json = json_encode($metadata); 
        }
        if(in_array($ext, $document_array)){
            $type = "document";
        }

    	$hash = hash_file('md5', $file);
    	$part1 = substr($hash, 0, 2);
    	$part2 = substr($hash, 2, 2);
    	$part3 = substr($hash, 4, 28);
    	$date = date_create();
    	$part4 = date_timestamp_get($date);

        $file->storeAs("{$part1}/{$part2}/", "{$part3}-{$part4}.{$ext}");
        $path = storage_path().'/app/'."{$part1}/{$part2}/{$part3}-{$part4}.{$ext}";

        if(file_exists($path)){ 
        $error = false;
        $msg = 'Uploaded successfully';

        $asset_db_save = new Asset;
        $asset_db_save->variation_of_id = null;
        $asset_db_save->acount_id = $acount_id;
        $asset_db_save->user_id= $user_id;
        $asset_db_save->title= $title;
        $asset_db_save->slug=$slug;
        $asset_db_save->description=$description;
        $asset_db_save->type=$type;
        $asset_db_save->mime_type=$mimetype;
        $asset_db_save->path=$path;
        $asset_db_save->hash=$hash;
        $asset_db_save->size=$size;
        $asset_db_save->is_link = $is_link;
        $asset_db_save->is_public=$is_public;
        $asset_db_save->session_type_enum=$session_type_enum;
        $asset_db_save->metadata=$metadata_json;
        $asset_db_save->source = $source;
        $asset_db_save->created_at = $created_at;
        $asset_db_save->save();
        $assetid = $asset_db_save->id;

        DB::table('asset_folder')->insert([
            ['asset_id' => $assetid, 'folder_id' => $father_id, 'created_at' => $created_at]
        ]);

        }else{
            $error = true;
            $msg = 'An error has ocurred, please try again later';
        }

        $folder_assets = $this->getNewAssets($father_id);
        return response()->json(['error' => $error, 'message'=>$msg, 'newAssets'=>$folder_assets]);
	}

    public function edit(Request $request){
        $this->validate(request(),[
            'item_id' => 'required',
            'item_type' => 'required',
            'new_name' => 'required',
            'new_description' => 'required'
            ]);
        $id = $request->input('item_id');
        $type = $request->input('item_type');
        $new_name = $request->input('new_name');
        $new_description = $request->input('new_description');
        $error = true;
        $msg = 'An error has ocurred, please try again later';
        
            $asset = Asset::find($id);
            $asset->title = $new_name;
            $asset->description = $new_description;
            if($asset->save()){
                $error = false;
                $msg = 'Asset updated';
            }           
        
        return response()->json(['error' => $error, 'message'=>$msg]);
    }


    public function getAssets($id=null){
        $id=$_GET['id'];
        $folder = Folder::find($id);
        $folder_asset = $folder -> assets;
        return response()->json(['folder' => $folder, 'asset'=>$folder_asset]);
    }

    public function download($id){
        $headers = array( 'Content-Type: application/octet-stream' );
        $assets = Asset::where('id' , $id) -> get(['path',  'title']);
        foreach ($assets as $asset){

            $title_array = explode(".", $asset->path);
            $title = $asset->title.'.'.$title_array[count($title_array)-1];

            return response()->download($asset->path, $title, $headers); 
        }
    }
    // query builder
    public function searchAssets(){

        $searchFor=trim($_REQUEST['searchFor']);

        $results = Asset::where('title', 'ilike', '%'.$searchFor.'%')
        ->get();  
        foreach($results as $result){
            $result -> isSelected = false;
            $folder = $result -> folders;
            
            $id=$folder['0']->id;
            
            $query = DB::select(DB::raw("WITH RECURSIVE
           children AS (SELECT
               id, title, parent_id, title::text as path
               FROM folders
               UNION SELECT
                   e.id, e.title, e.parent_id,
                       (s.path || ' / ' || e.title)::text as path
               FROM folders e
               INNER JOIN children s on s.id = e.parent_id
           ) SELECT  * from children where id = ".$id
           ));
            

            $full_path=$query[count($query)-1]->path;
            $result ->father = $full_path;

        }      

        return response()->json(['error'=>false, 'msg'=>'', 'asset'=>$results]);        
    }

    public function copy(Request $request){
        $error = true;
        $msg = 'An error has ocurred, please try again later';
        $father_id = $_REQUEST['father_id'];

        $item = json_decode($_REQUEST['item']);
        $asset_db_save = new Asset;
        $asset_db_save->variation_of_id = null;
        $asset_db_save->acount_id = $item->acount_id;
        $asset_db_save->user_id= $item->user_id;
        $asset_db_save->title= $item->title;
        $asset_db_save->slug=$item->slug;
        $asset_db_save->description=$item->description;
        $asset_db_save->type=$item->type;
        $asset_db_save->mime_type=$item->mime_type;
        $asset_db_save->path=$item->path;
        $asset_db_save->hash=$item->hash;
        $asset_db_save->size=$item->size;
        $asset_db_save->is_link = $item->is_link;
        $asset_db_save->is_public=$item->is_public;
        $asset_db_save->session_type_enum=$item->session_type_enum;
        $asset_db_save->metadata=$item->metadata;
        $asset_db_save->source = $item->source;
        $asset_db_save->created_at = date('Y-m-d H:i:s');
        
        if($asset_db_save->save()){
            $error = false;
            $msg = 'success';
        }
        
        $asset_id = $asset_db_save->id;

        DB::table('asset_folder')->insert([
            ['asset_id' => $asset_id, 'folder_id' => $father_id, 'created_at' => date('Y-m-d H:i:s')]
        ]);

        $folder_assets = $this->getNewAssets($father_id);        
        return response()->json(['error' => $error, 'message'=>$msg, 'assets'=>$folder_assets]);
    }

    public function cut(Request $request){

        $error = true;
        $msg = 'An error has ocurred, please try again later';
        $father_id = $_REQUEST['father_id'];
        $item = json_decode($_REQUEST['item']);
    
        if(DB::table('asset_folder')->where('asset_id', $item->id )->update(['folder_id'=>$father_id, 'updated_at'=>date('Y-m-d H:i:s')])){
            $error = false;
            $msg = 'success';
        }

        $folder_assets = $this->getNewAssets($father_id);
        return response()->json(['error' => $error, 'message'=>$msg, 'assets'=>$folder_assets]);
    }

    public function delete(Request $request){
        $error = true;
        $msg = 'An error has ocurred, please try again later';
        $item = json_decode($_REQUEST['item']);

        if(DB::table('asset_folder')->where('asset_id', $item->id )->update(['folder_id'=>5, 'updated_at'=>date('Y-m-d H:i:s'), 'trashed_at'=>date('Y-m-d H:i:s')])){
            $error = false;
            $msg = 'success';
        }
         return response()->json(['error' => $error, 'message'=>$msg]);

    }

    public  function getNewAssets($father_id){
         $folder = Folder::find($father_id);
        $folder_assets = $folder -> assets;

        foreach($folder_assets as $folder_asset){
            $folder_asset -> isSelected = false;
            $folder = $folder_asset -> folders;
            $folder_asset -> father = $folder;
        } 
        return $folder_assets;
    }

}
