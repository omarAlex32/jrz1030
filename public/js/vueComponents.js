///////////////////////////////////////////////////////////////////////////////////////////////////
// start of folder component
Vue.component('folder',{
	props:{
		folder:{},
		view:'',
		searching:'',
	},
	template:`
		<div>
			<div>
				<div :class="{'folder-icon':true, 'is-selected': folder.isSelected}" :id="folder.id" :key="folder.id" @click="select()">
					<i class="glyphicon glyphicon-folder-open"> </i>  {{folder.title}}
				</div>
			</div>

			
		</div>		
	`,
	methods:{
		select(e){
			window.event.stopPropagation();
			if(!window.event.ctrlKey){
				Event.$emit('clear-selection');
			}
			this.folder.isSelected = !this.folder.isSelected;
			Event.$emit('add-to-selection', this.folder, 'folder');
		},
	}
});
///////////////////////////////////////////////////////////////////////////////////////////////////
// end of folder component

///////////////////////////////////////////////////////////////////////////////////////////////////
// start of asset component
Vue.component('asset', {
	props:{
		asset:{},
		view:'',
		searching:'',
	},
	template:`
		<div v-show="asset.trashed==null">
			<div v-show="view&&!searching">
				
					<div :class="{'Thumbnail':true, 'asset-icon':true, 'is-selected': asset.isSelected}" :id="asset.id" :key="asset.id" @click="select()">
		  				<h1 class="text-center"><i :class="getType(asset.type)"></i></h1>
		  				<div class="caption">
			    			<h6>{{asset.title}}</h6>
			    			<p>{{asset.type}}</p>
			    			</div>
		    		</div>
				
			</div>

			<div v-show="!view||searching">
				<div :class="{'row':true, 'asset-list':true, 'text-left':true, 'is-selected': asset.isSelected}" @click="select()">
					<div :class="{'col-xs-5':!searching, 'col-xs-4':searching}"><i :class="getType(asset.type)"></i> {{asset.title}}</div>
					<div :class="{'col-xs-3':!searching, 'col-xs-2':searching}">{{asset.type}}</div>
					<div :class="{'col-xs-4':!searching, 'col-xs-3':searching}">{{getSize(asset.size)}}</div>
					<div v-show="searching" :class="{'col-xs-4':!searching, 'col-xs-3':searching}">{{asset.father}}</div>
				</div>
				
			</div>
		</div>
	`,
	
	methods:{
		getSize(sizeInBytes){
			var type = "bytes"; 
			var size = parseFloat(sizeInBytes);
			if(size>=1024){
				type = "Kbs";
				size = size / 1024;
			}
			if(size>=1048576){
				type = "Mbs";
				size = size / 1048576;
			}
			if(size>=1073741824 ){
				type = "Gbs";
				size = size / 1073741824;
			}
			var fullSize = size.toFixed(3) +" "+ type;
			return fullSize;
		},
		select(e){
			window.event.stopPropagation();
			if(!window.event.ctrlKey){
				Event.$emit('clear-selection');
			}
			this.asset.isSelected = !this.asset.isSelected;
			Event.$emit('add-to-selection', this.asset, 'asset');
		},
		getType(type){
			switch(type){
				case 'image':
				return 'glyphicon glyphicon-picture';
				break;
				case 'video':
				return 'glyphicon glyphicon-facetime-video';
				break;
				case 'document':
				return 'glyphicon glyphicon-file';
				break;
				case 'audio':
				return 'glyphicon glyphicon-headphones';
				break;
				
			}
		}
	}	

});
///////////////////////////////////////////////////////////////////////////////////////////////////
// end of asset component


// modals////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
// start of creeate folder modal component
Vue.component('create-folder-modal',{
	props:['title', 'father_id'],
	data(){
		return{
			message:'',
			alertType:'',
			alertShow:false,
		}
	},
	template:`
			<div> 
				<div class="modal show"  tabindex="-1" role="dialog">
			  		<div class="modal-dialog" role="document">
			    		<div class="modal-content">
			      		<form  @submit.prevent = "submit" id="create_folder_form" action="" enctype="multipart/form-data">
			      			<div class="modal-header">
			        			<button type="button" class="close" @click="$emit('close'), alertShow=false"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
			       	 			<h4 class="modal-title">Create folder in {{title}}</h4>
			      			</div>
			      			<div class="modal-body">
			      			   	<input type="hidden" name="father_id" id="father_id" :value="father_id"/>
			      				<div class="form-group">
			    					<label for="folder_name">Name</label>
			    					<input type="text" class="form-control" onClick="this.select();" id="folder_name" name="folder_name"  placeholder="Enter folder name" required>
			  					</div>
			  					<div class="form-group">
			    					<label for="folder_description">Description</label>
			    					<textarea class="form-control" id="folder_description" name="folder_description"  rows="3" required></textarea>
			  					</div>
			        			<div v-if="alertShow">
			        			<div  :class="alertType">
			    					<a href="#" class="close" @click="alertShow=false">&times;</a>
			    					<strong>{{message}}</strong> 
								</div>
								</div>
			     			</div>
			     			<div class="modal-footer">
			       				<button type="button" class="btn btn-default" @click="$emit('close'), alertShow=false" >Close</button>
			      				<button type="submit" class="btn btn-primary">Create</button>
			     			</div>
			        	</form>
			      	</div><!-- /.modal-content -->
			  	</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
			</div>
	`,
	// updated(){
	// 	console.log(this.name);
	// },
	methods:{
		submit:function(){
			var form = document.getElementById('create_folder_form');
			var request = new XMLHttpRequest();
			var formdata = new FormData(form);
			var messageTemp = "";
			var alertTypeTemp = "";
			var father_id = this.father_id;
			var title = $('#folder_name').val();
			
			$.ajax({
				headers:
	            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
	            url:'/folders/create',
	     	 	data: formdata,
	      		async:false,
	      		type:'post',
	      		processData: false,
	      		contentType: false,
	      		success:function(response){
	      			if(response.error==false){
	        			app.folders=response.folders;
	        			jQuery("#create_folder_form")[0].reset();
	        			messageTemp = response.message;
	        			alertTypeTemp = "alert alert-success";
	        			if($('.node-'+father_id).attr('state')=='open'){
	        				$('.node-'+father_id).append('<details><summary class="summ-'+response.folder_id+'"  onclick="getNodesNFiles('+response.folder_id+');">'+title+'</summary><div state="closed" class="node-'+response.folder_id+'" style="margin-left:15px;"></div></details>');
	        			}
	        		}else{
	        			messageTemp = response.message;
	        			alertTypeTemp = "alert alert-danger";
	        		}
	        	},
	        	error:function(error){
	        		console.log(error);
	        	}
	        });
			this.message = messageTemp;
			this.alertType = alertTypeTemp;
			this.alertShow = true;
		}
	}
});
///////////////////////////////////////////////////////////////////////////////////////////////////
// end of create folder modal component

///////////////////////////////////////////////////////////////////////////////////////////////////
// start of upload-modal component
Vue.component('upload-modal', {
	props:['title', 'father_id'],
	data(){
		return{
			message:'',
			alertType: '',
			alertShow: false,
			}
	},
	template: `
			<div> 
				<div class="modal show"  tabindex="-1" role="dialog">
			  		<div class="modal-dialog" role="document">
			    		<div class="modal-content">
			      		<form  @submit.prevent = "submit" id="upload_form" action="" enctype="multipart/form-data">
			      			<div class="modal-header">
			        			<button type="button" class="close" @click="$emit('close'), alertShow=false"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
			       	 			<h4 class="modal-title">Upload file to {{title}}</h4>
			      			</div>
			      			<div class="modal-body">
			      			   	<input type="hidden" name="father_id" id="father_id" :value="father_id"/>
			      				<div class="form-group">
			    					<label for="file_name">Name</label>
			    					<input type="text" class="form-control" id="file_name" name="file_name"  placeholder="Enter file name" required>
			  					</div>
			  					<div class="form-group">
			    					<label for="file_description">Description</label>
			    					<textarea class="form-control" id="file_description" name="file_description"  rows="3" required></textarea>
			  					</div>
			  					<div class="form-group">
			    					<label for="file">File</label>
			    					<input type="file" class="form-control-file" id="file" name="file"  required >
			  					</div>       
			        			<div v-if="alertShow">
			        			<div  :class="alertType">
			    					<a href="#" class="close" @click="alertShow=false">&times;</a>
			    					<strong>{{message}}</strong> 
								</div>
								</div>
			     			</div>
			     			<div class="modal-footer">
			       				<button type="button" class="btn btn-default" @click="$emit('close'), alertShow=false" >Close</button>
			      				<button type="submit" class="btn btn-primary">Upload</button>
			     			</div>
			        	</form>
			      	</div><!-- /.modal-content -->
			  	</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
			</div>
	`,
	methods:{
		submit:function(){
			var form = document.getElementById('upload_form');
			var request = new XMLHttpRequest();
			var formdata = new FormData(form);
			var messageTemp = "";
			var alertTypeTemp = "";
			
			$.ajax({
				headers:
	            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
	            url:'/assets/upload',
	     	 	data: formdata,
	      		async:false,
	      		type:'post',
	      		processData: false,
	      		contentType: false,
	      		success:function(response){
	      			if(response.error==false){
	        			app.files=response.newAssets;
	        			jQuery("#upload_form")[0].reset();
	        			messageTemp = response.message;
	        			alertTypeTemp = "alert alert-success";
	        			Event.$emit('clear-selection');
	        		}else{
	        			messageTemp = response.message;
	        			alertTypeTemp = "alert alert-danger";
	        		}
	        	},
	        	error:function(error){
	        		console.log(error);
	        	}
	        });
			this.message = messageTemp;
			this.alertType = alertTypeTemp;
			this.alertShow = true;
		}
	}
});
///////////////////////////////////////////////////////////////////////////////////////////////////
// end of upload-modal component


///////////////////////////////////////////////////////////////////////////////////////////////////
// start of rename modal component
Vue.component('rename-modal',{
	props:['title', 'item_id', 'item_type', 'item_description'],
	data(){
		return{
			message:'',
			alertType:'',
			alertShow:false,
			name: this.title,
			description: this.item_description
		}
	},
	template:`
			<div> 
				<div class="modal show"  tabindex="-1" role="dialog">
			  		<div class="modal-dialog" role="document">
			    		<div class="modal-content">
			      		<form  @submit.prevent = "submit" id="rename_form" action="" enctype="multipart/form-data">
			      			<div class="modal-header">
			        			<button type="button" class="close" @click="$emit('close'), alertShow=false"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
			       	 			<h4 class="modal-title">Rename</h4>
			      			</div>
			      			<div class="modal-body">
			      			   	<input type="hidden" name="item_id" id="item_id" :value="item_id"/>
			      			   	<input type="hidden" name="item_type" id="item_type" :value="item_type"/>
			      				<div class="form-group">
			    					<label for="new_name">New name</label>
			    					<input type="text" class="form-control" onClick="this.select();" id="new_name" name="new_name" v-model="name" placeholder="Enter new file name" required>
			  					</div>
			  					<div class="form-group">
			    					<label for="new_description">Description</label>
			    					<textarea class="form-control" id="new_description" name="new_description"  rows="3" v-model="description" required></textarea>
			  					</div>
			        			<div v-if="alertShow">
			        			<div  :class="alertType">
			    					<a href="#" class="close" @click="alertShow=false">&times;</a>
			    					<strong>{{message}}</strong> 
								</div>
								</div>
			     			</div>
			     			<div class="modal-footer">
			       				<button type="button" class="btn btn-default" @click="$emit('close'), alertShow=false" >Close</button>
			      				<button type="submit" class="btn btn-primary">Save</button>
			     			</div>
			        	</form>
			      	</div><!-- /.modal-content -->
			  	</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
			</div>
	`,
	// updated(){
	// 	console.log(this.name);
	// },
	methods:{
		submit:function(){
			var form = document.getElementById('rename_form');
			var request = new XMLHttpRequest();
			var formdata = new FormData(form);
			var name = this.name;
			var description = this.description;
			var type = this.item_type;
			var id = this.item_id;
			var messageTemp = "";
			var alertTypeTemp = "";
			var new_title = "";
			var url ="";
	      	if(type=='asset'){
	      		url = '/assets/rename';
	      	}else{
	      		url = '/folders/rename'
	      	}

			$.ajax({
				headers:
	            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
	            url: url,
	     	 	data:formdata,
	      		async:false,
	      		type:'post',
	      		processData: false,
	      		contentType: false,
	      		success:function(response){
	      			if(response.error==false){
	        			messageTemp = response.message;
	        			alertTypeTemp = "alert alert-success";
	        			Event.$emit('rename-asset-folder', name, type, id, description);
	        		}else{
	        			messageTemp = response.message;
	        			alertTypeTemp = "alert alert-danger";
	        		}
	        	},
	        	error:function(error){
	        		console.log(error);
	        	}
	        });
			this.message = messageTemp;
			this.alertType = alertTypeTemp;
			this.alertShow = true;
			
		}
	}
});
///////////////////////////////////////////////////////////////////////////////////////////////////
// end of rename modal component

