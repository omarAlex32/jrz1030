<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Folder;
use App\Asset;


class FoldersController extends Controller
{
    public function store(Request $request){
        //validate form data
        $this->validate(request(),[
            'folder_name' => 'required',
            'folder_description' => 'required',
        ]);

        $father_id = $request->input('father_id');
        $description = $request->input('folder_description');
        $title = $request->input('folder_name');
        $error = true;
        $msg = 'An error has ocurred, please try again later';
        $folder_id = 0;


        $folder_db_save = new Folder;
        $folder_db_save->parent_id = $father_id;
        $folder_db_save->account_id = '001';
        $folder_db_save->user_id = '001';
        $folder_db_save->title = $title;
        $folder_db_save->slug = 'trtr';
        $folder_db_save->description = $description;
        $folder_db_save->size = '0';
        $folder_db_save->item_count = '0';
        $folder_db_save->folder_type_enum = '0';
        $folder_db_save->created_at = date('Y-m-d H:i:s');
        
        if($folder_db_save->save()){
            $folder_id = $folder_db_save->id;
            $error = false;
            $msg = 'Folder created successfully';
        }

        $folders = Folder::where('parent_id', $father_id)->get();
        
        foreach($folders as $folder){
            $folder->isSelected = false;
        } 
        
        return response()->json(['error' => $error, 'message'=>$msg, 'folders'=>$folders, 'folder_id'=>$folder_id]);
        }


    public function edit(Request $request){
        $this->validate(request(),[
            'item_id' => 'required',
            'item_type' => 'required',
            'new_name' => 'required',
            'new_description' => 'required'
            ]);
        $id = $request->input('item_id');
        $type = $request->input('item_type');
        $new_name = $request->input('new_name');
        $new_description = $request->input('new_description');
        $error = true;
        $msg = 'An error has ocurred, please try again later';
        
            $folder = Folder::find($id);
                $folder->title = $new_name;
                $folder->description = $new_description;
                if($folder->save()){
                    $error = false;
                    $msg = 'Folder updated';
            }           

        return response()->json(['error' => $error, 'message'=>$msg]);
    }


    public function loadTree(){
    	if(isset($_GET['id'])){
    		$idtofind=$_GET['id'];
    	}else{
    		$idtofind=null;
    	}
    	$folders = Folder::where('parent_id', $idtofind)->get();
    	return $folders;
    }

    public function getFolders($id=null){
        $id=$_GET['id'];
        $folder = Folder::where('parent_id', $id)->get();
        return response()->json(['folder'=>$folder]);
    }

    public function cut(Request $request){
        $error = true;
        $msg = 'An error has ocurred, please try again later';
        $father_id = $_REQUEST['father_id'];
        $item = json_decode($_REQUEST['item']);
    
        if(Folder::where('id', $item->id )->update(['parent_id'=>$father_id, 'updated_at'=>date('Y-m-d H:i:s')])){
            $error = false;
            $msg = 'success';
        }

        $folders = Folder::where('parent_id', $father_id)->get();
        foreach($folders as $folder){
            $folder->isSelected = false;
        } 
        return response()->json(['error' => $error, 'message'=>$msg, 'folders'=>$folders]);
    }

    public function copy(Request $request){
        $error = true;
        $msg = 'An error has ocurred, please try again later';
        $father_id = $_REQUEST['father_id'];
        $item = json_decode($_REQUEST['item']);
    }
    
}
