window.Event = new Vue();


///////////////////////////////////////////////////////////////////////////////////////////////////
// start of principal vue instance
var app = new Vue({
	el: ".container",
	data:{
		folder:{
			id:'', 
			title:'Content', 
			type:'null'
		},
		files: [],
		folders: [],
		showUploadModalVar:false,
		showCreateFolderModalVar:false,
		showRenameModalVar:false,
		selection:[],
		isSearching:false,
		showContenMessage:true,
		viewVar:true,
		clipboard:{
			action:'',
			items:[]
		},
		// selectionSize:0,
		
	},
	updated(){
		if(this.files.length<=0){
			this.showContenMessage=true;
		}else{
			this.showContenMessage=false;
		}

	},
	created(){
		Event.$on('clear-selection', ()=> this.clearSelection());
		Event.$on('add-to-selection' , (item, type) => this.addToSelection(item, type));
		Event.$on('rename-asset-folder', (name, type, id,description) => this.renameAssetFolder(name,type,id,description));
	},
	methods:{
		clipboardActions(action){
			switch(action){
				case 'cut':
					if(!this.isFileSelected){
						// console.log('cut');
						this.clipboard = [];
						this.clipboard.action = 'cut'
						this.clipboard.items = this.selection;
						this.clearSelection();
						// console.log(this.clipboard)

					}
				break;
				case 'copy':

					if(!this.isFileSelected){
						// console.log('copy');
						this.clipboard = [];
						this.clipboard.action = 'copy'
						this.clipboard.items = this.selection;
						this.clearSelection();
						// console.log(this.clipboard)

					}
				break;
				case 'paste':
					if(!this.isClipboardEmpty){
						this.clipboard.items.forEach(function(item, index){		
							if(app.clipboard.action == 'copy'){
								if(item.itemType=="asset"){
									url = '/assets/copy';
								}else{
									url = '/folders/copy';
								}
							}else{
								if(item.itemType=="asset"){
									url = '/assets/cut';
								}else{
									url = '/folders/cut';
								}
							}
							$.ajax({
								headers:
								{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
								url:url,
								data: 'item='+JSON.stringify(item)+'& father_id='+  app.folder.id,
								type:'get',
								processData: false,
								contentType: false,
								success:function(response){
									if(response.error==false){
										if(item.itemType=="asset"){
											app.files = response.assets;
										}else{
											app.folders = response.folders;
											if($('.node-'+item.parent_id).attr('state')=='open'){
												$('.summ-'+item.id).parent().remove();
											}
											if($('.node-'+app.folder.id).attr('state')=='open'){
						        				$('.node-'+app.folder.id).append('<details><summary class="summ-'+item.id+'"  onclick="getNodesNFiles('+item.id+');">'+item.title+'</summary><div state="closed" class="node-'+item.id+'" style="margin-left:15px;"></div></details>');
						        			}
										}
									}else{
										console.log(response.msg);
									}
										
								},
								error:function(error){
									console.log(error);
								}
							});		
						});	
					}
				break;
			}
		},
		deleteItem(){
			// console.log('delete');
			bootbox.confirm({ 
				size: "small",
				title: "Delete items",
				message: "Are you sure?", 
				callback: function(result){
					if (result) {
						app.selection.forEach(function(item, index){
							if(item.itemType=='asset'){
								$url= '/assets/delete';
							}else{
								$url= '/folders/delete';
							}
							$.ajax({
								headers:
								{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
								url: $url,
								data: 'item='+JSON.stringify(item)+'& father_id='+  app.folder.id,
								type:'get',
								processData: false,
								contentType: false,
								success:function(response){
									if(response.error==false){
										item.trashed='trashed';
										app.selectAll();
										app.clearSelection();
										
									}else{
										console.log(response.msg)
									}
								},
								error:function(error){
									console.log(error);
								}
							});
						})
					} 
				}
			})
			
		},
		renameAssetFolder(name, type, id, description){
			if(type == 'folder'){
				$(".summ-"+id).html(name);
			}
			this.selection[0].title = name;
			this.selection[0].description = description;
		},
		showModal(modal){
			switch(modal){
				case 'upload':
					if(this.isInFolder){
					this.showUploadModalVar = true;
					}
				break;
				case 'rename':
					if(this.multipleFiles){
					this.showRenameModalVar = true;
					}
				break;
				case 'create_folder':
					if(this.isInFolder){
					this.showCreateFolderModalVar = true;
					}
				break;
			}
			
		},
		goToFolder(id){
			loadPrimaryTree();
			getNodesNFiles(id);
		},
		downloadAsset(id){
			window.location.href="/assets/download/"+id;
		},
		clearSelection(){
			this.selection = [];
			this.selectionSize = 0;
			for(var i=0; i < this.files.length; i++){
     			this.files[i].isSelected=false;
      		}
      		for(var i=0; i < this.folders.length; i++){
     			this.folders[i].isSelected=false;
      		}
		},
		addToSelection(item, type){
			// var sizeFloat = parseFloat(size);
			
			var index = this.selection.indexOf(item);
			if(index != -1){
				this.selection.splice(index, 1);
				// this.selectionSize = (this.selectionSize - sizeFloat);
			}else{
			item.itemType = type;	
			this.selection.push(item);
			// this.selectionSize = (this.selectionSize + sizeFloat);
			}
		},
		selectAll(){
			this.clearSelection();
			this.files.forEach(function(item, index, arr){
				arr[index].isSelected=true;
				item.itemType = 'asset';
				app.selection.push(item)
			});
			this.folders.forEach(function(item, index, arr){
				arr[index].isSelected=true;
				item.itemType = 'folder';
				app.selection.push(item)
			});
		},
		switchSearching(swap){
			if(swap){
				this.isSearching=false;
				this.folder.title="Content";
				loadPrimaryTree();
				jQuery("#search_form")[0].reset();
			}else{
				this.isSearching=true;
				this.folder.title="Search results";

			}
			this.folder.id="";
			this.folder.type="null";
			this.files = [];
			this.folders= [];
			this.clearSelection();
		},
		search(){
			$.ajax({
				headers:
	            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
	            url:'/assets/search',
	     	 	data: new FormData(jQuery("#search_form")[0]),
	      		type:'post',
	      		processData: false,
	      		contentType: false,
	      		success:function(response){
	      			if(response.error==false){
	        			app.files = response.asset;
	        		}else{
	        			console.log(response.msg)
	        		}
	        	},
	        	error:function(error){
	        		console.log(error);
	        	}
	        });
		},
	},
	computed:{
		isInFolder:function(){
			if(this.folder.type != 2 && this.folder.type != 'null'){
				return true
			}else{
				return false
			}
		},
		isSystemFolder:function(){
			if(this.folder.type != 0 && this.folder.type != 'null' && this.folder.type != 2){
				return true;
			}else{
				return false;
			}
		},
		isTrashFolder:function(){
			if(this.folder.type == 2){
				return false;
			}else{
				return true;
			}
		},
		isFileSelected:function(){
			if(this.selection.length<1){
				return true;
			}else{
				return false;
			}
		},
		multipleFiles:function(){
			if(this.selection.length > 1 || this.selection.length <= 0){
				return false;
			}else{
				return true;
			}
		},
		isClipboardEmpty:function(){
			if(this.clipboard.items.length<=0){
				return true;
			}else if(this.isSearching||this.folder.id==''){
				return true;
			}else{
				return false;
			}
		},
		areFiles(){
			if(this.files.length<=0){
				return false;
			}else{
				return true;
			}
		},
		// getSize:function(sizeInBytes){
		// 	var type = "bytes"; 
		// 	var size = sizeInBytes;
		// 	if(size>=1024){
		// 		type = "Kbs";
		// 		size = size / 1024;
		// 	}
		// 	if(size>=1048576){
		// 		type = "Mbs";
		// 		size = size / 1048576;
		// 	}
		// 	if(size>=1073741824 ){
		// 		type = "Gbs";
		// 		size = size / 1073741824;
		// 	}
		// 	var fullSize = size.toFixed(3) +" "+ type;
		// 	return fullSize;
		// },
	},		
});
///////////////////////////////////////////////////////////////////////////////////////////////////
// end of principal vue instance